package main

import (
	"bufio"
	"fmt"
	"github.com/spf13/viper"
	"log"
	"os"
)

func main() {

	appName := "go-parse-logs"
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("/etc/" + appName)
	viper.AddConfigPath("$HOME/." + appName)
	viper.AddConfigPath(".")

	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Unable to load config file %s \n", err))
	}

	fmt.Println(viper.Get("app"))

	fileName := viper.GetString("app.file-name")
	fmt.Printf("file to process %v\n", fileName)

	file, err := os.Open(fileName)
	if err != nil {
		log.Fatal("Unable to open file", err)
	}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}

	defer file.Close()



}
